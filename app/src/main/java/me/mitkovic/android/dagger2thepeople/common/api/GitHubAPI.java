package me.mitkovic.android.dagger2thepeople.common.api;

import java.util.List;

import io.reactivex.Observable;
import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;
import retrofit2.Call;
import retrofit2.http.GET;

public interface GitHubAPI {

    //Simple
    @GET("repositories")
    Call<List<GithubRepo>> getAllRepos();

    //Rx
    @GET("repositories")
    Observable<List<GithubRepo>> getObservableRepos();

}
