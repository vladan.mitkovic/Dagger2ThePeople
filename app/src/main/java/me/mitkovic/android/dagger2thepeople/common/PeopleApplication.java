package me.mitkovic.android.dagger2thepeople.common;

import android.app.Activity;
import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.facebook.drawee.backends.pipeline.Fresco;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.HasActivityInjector;
import me.mitkovic.android.dagger2thepeople.common.di.DaggerApplicationComponent;

public class PeopleApplication extends Application implements HasActivityInjector {

    @Inject
    AndroidInjector<Activity> activityInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        setup();
    }

    protected void setup() {
        DaggerApplicationComponent
                .builder()
                .application(this)
                .build()
                .inject(this);

        Fresco.initialize(this);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }

}
