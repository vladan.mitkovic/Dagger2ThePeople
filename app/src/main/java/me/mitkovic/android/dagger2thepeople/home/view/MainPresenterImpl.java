package me.mitkovic.android.dagger2thepeople.home.view;

import android.os.Bundle;

import java.util.Locale;

import javax.inject.Inject;

import icepick.Icepick;
import me.mitkovic.android.dagger2thepeople.BuildConfig;
import me.mitkovic.android.dagger2thepeople.common.Navigation;

public class MainPresenterImpl implements MainPresenter {
    private static final String TAG = MainPresenterImpl.class.getSimpleName();

    private MainView view;

    private final Navigation navigation;

    @Inject
    public MainPresenterImpl(Navigation navigation) {
        this.navigation = navigation;
    }

    @Override
    public void onBind(MainView view) {
        this.view = view;

        view.setVersion(String.format(Locale.ENGLISH, "%s (%s)", BuildConfig.VERSION_NAME, BuildConfig.SHA1));
    }

    @Override
    public void onClickSimpleExample() {
        navigation.viewSimpleExample();
    }

    @Override
    public void onClickRxJava2Example() {
        navigation.viewRxJava2Example();
    }

    @Override
    public void onUnbind() {
        this.view = null;
    }

    @Override
    public void onRestore(Bundle bundle) {
        Icepick.restoreInstanceState(this, bundle);
    }

    @Override
    public void onSave(Bundle bundle) {
        Icepick.saveInstanceState(this, bundle);
    }

}
