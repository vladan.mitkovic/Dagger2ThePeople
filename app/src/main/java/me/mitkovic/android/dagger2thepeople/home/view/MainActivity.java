package me.mitkovic.android.dagger2thepeople.home.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import hugo.weaving.DebugLog;
import me.mitkovic.android.dagger2thepeople.R;

public class MainActivity extends AppCompatActivity implements MainView {

    @Inject
    MainPresenter presenter;

    @BindView(R.id.appVersion)
    TextView versionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (savedInstanceState != null) {
            presenter.onRestore(savedInstanceState);
        }
        presenter.onBind(this);
    }

    @Override
    @DebugLog
    public void setVersion(String version) {
        versionView.setText(version);
    }

    @OnClick(R.id.simpleExample)
    void onClickSimpleExample() {
        presenter.onClickSimpleExample();
    }

    @OnClick(R.id.rxJava2Example)
    void onClickRxJava2Example() {
        presenter.onClickRxJava2Example();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null)
            presenter.onSave(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.onUnbind();
    }
}
