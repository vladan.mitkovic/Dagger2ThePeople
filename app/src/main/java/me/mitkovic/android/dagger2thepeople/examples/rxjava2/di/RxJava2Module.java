package me.mitkovic.android.dagger2thepeople.examples.rxjava2.di;


import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.dagger2thepeople.common.Navigation;
import me.mitkovic.android.dagger2thepeople.common.NavigationImpl;
import me.mitkovic.android.dagger2thepeople.common.di.scope.PerActivity;
import me.mitkovic.android.dagger2thepeople.examples.rxjava2.view.RxJava2Activity;
import me.mitkovic.android.dagger2thepeople.examples.rxjava2.view.RxJava2Presenter;
import me.mitkovic.android.dagger2thepeople.examples.rxjava2.view.RxJava2PresenterImpl;

@PerActivity
@Module
public abstract class RxJava2Module {

    @Binds
    abstract RxJava2Presenter bindsSimplePresenter(RxJava2PresenterImpl presenter);

    @PerActivity
    @Provides
    public static Navigation providesNavigation(RxJava2Activity rxJava2Activity) {
        return new NavigationImpl(rxJava2Activity);
    }

}
