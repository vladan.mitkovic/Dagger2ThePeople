package me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;

public class AdapterRepos extends BaseAdapter {

    private List<GithubRepo> repoList = new ArrayList<>();
    private Context context;
    private Picasso picasso;

    private Listener listener;

    public interface Listener {
        void onClickRepoRow(GithubRepo githubRepo);
    }

    @Inject
    public AdapterRepos(SimpleActivity context, Picasso picasso) {
        this.context = context;
        this.picasso = picasso;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return repoList.size();
    }

    @Override
    public GithubRepo getItem(int position) {
        return repoList.get(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        return repoList.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RepoListItem repoListItem;
        if (convertView == null) {
            repoListItem = new RepoListItem(context, picasso);
        } else {
            repoListItem = RepoListItem.class.cast(convertView);
        }

        final GithubRepo githubRepo = repoList.get(position);

        repoListItem.setRepo(githubRepo);

        repoListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener == null) return;
                listener.onClickRepoRow(githubRepo);
            }
        });

        return repoListItem;
    }

    public void swapData(List<GithubRepo> githubRepos) {
        repoList.clear();
        if (githubRepos != null) {
            repoList.addAll(githubRepos);
        }
        notifyDataSetChanged();
    }

    public void clearData() {
        repoList.clear();
        notifyDataSetChanged();
    }

}
