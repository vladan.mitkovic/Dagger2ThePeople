
package me.mitkovic.android.dagger2thepeople.common.api.model;

public class GithubRepoOwner {

    public long id;

    public String login;

    public String avatar_url;

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getAvatar() {
        return avatar_url;
    }

    @Override
    public String toString() {
        return "GithubRepoOwner{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", avatar_url='" + avatar_url + '\'' +
                '}';
    }
}
