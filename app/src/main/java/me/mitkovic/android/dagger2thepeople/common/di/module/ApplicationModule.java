package me.mitkovic.android.dagger2thepeople.common.di.module;

import android.app.Activity;
import android.content.Context;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import me.mitkovic.android.dagger2thepeople.common.PeopleApplication;
import me.mitkovic.android.dagger2thepeople.common.di.scope.ApplicationContext;
import me.mitkovic.android.dagger2thepeople.common.di.scope.PeopleApplicatonScope;

@Module
public abstract class ApplicationModule {

    @PeopleApplicatonScope
    @Binds
    abstract AndroidInjector<Activity> bindsActivityInjector(DispatchingAndroidInjector<Activity> injector);

    @Provides
    @PeopleApplicatonScope
    @ApplicationContext
    public static Context providesContext(PeopleApplication application) {
        return application.getApplicationContext();
    }

}
