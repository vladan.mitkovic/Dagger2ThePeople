
package me.mitkovic.android.dagger2thepeople.common.api.model;

import org.joda.time.DateTime;

public class GithubRepo {

  public long id;

  public String name;

  public GithubRepoOwner owner;

  public String description;

  public DateTime updated_at;

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public GithubRepoOwner getOwner() {
    return owner;
  }

  public String getDescription() {
    return description;
  }

  public DateTime getUpdatedAt() {
    return updated_at;
  }

  @Override
  public String toString() {
    return "GithubRepo{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", owner=" + owner +
            ", updated_at=" + updated_at +
            '}';
  }
}
