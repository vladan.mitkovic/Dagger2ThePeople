package me.mitkovic.android.dagger2thepeople.common;

import android.app.Activity;
import android.content.Intent;

import me.mitkovic.android.dagger2thepeople.examples.rxjava2.view.RxJava2Activity;
import me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view.SimpleActivity;

public class NavigationImpl implements Navigation {

    private final Activity activity;

    public NavigationImpl(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void viewSimpleExample() {
        Intent qaIntent = new Intent(activity, SimpleActivity.class);
        activity.startActivity(qaIntent);
    }

    @Override
    public void viewRxJava2Example() {
        Intent qaIntent = new Intent(activity, RxJava2Activity.class);
        activity.startActivity(qaIntent);
    }

}
