package me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view;

import java.util.List;

import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;
import me.mitkovic.android.dagger2thepeople.common.view.View;

public interface SimpleView extends View {

    void setAdapterData(List<GithubRepo> githubRepos);

    void setSuccessSnackBar();

    void setErrorMessage(String errorMessage);

    void setLoading(boolean isLoading);

}
