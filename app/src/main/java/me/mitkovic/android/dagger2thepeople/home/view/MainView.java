package me.mitkovic.android.dagger2thepeople.home.view;

import me.mitkovic.android.dagger2thepeople.common.view.View;

public interface MainView extends View {

    void setVersion(String version);

}
