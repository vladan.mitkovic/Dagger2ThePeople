package me.mitkovic.android.dagger2thepeople.examples.rxjava2.view;

import me.mitkovic.android.dagger2thepeople.common.view.Presenter;

public interface RxJava2Presenter extends Presenter<RxJava2View> {

    void onClickRefresh();

}
