package me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.mitkovic.android.dagger2thepeople.R;
import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;

@SuppressLint("ViewConstructor")
public class RepoListItem extends FrameLayout {

    Context context;
    Picasso picasso;

    @BindView(R.id.user_avatar)
    ImageView avatarImage;

    @BindView(R.id.repo_name)
    TextView name;

    @BindView(R.id.repo_description)
    TextView description;

    @BindView(R.id.repo_updated_at)
    TextView updatedAt;

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.fullDate();

    public RepoListItem(Context context, Picasso picasso) {
        super(context);
        this.context = context;
        this.picasso = picasso;
        inflate(getContext(), R.layout.list_item_repo, this);
        ButterKnife.bind(this);
    }

    public void setRepo(GithubRepo githubRepo) {
        name.setText(githubRepo.getName());
        description.setVisibility(TextUtils.isEmpty(githubRepo.getDescription()) ? GONE : VISIBLE);
        description.setText(githubRepo.getDescription());

        updatedAt.setText(DATE_TIME_FORMATTER.print(githubRepo.getUpdatedAt()));

        picasso.load(githubRepo.getOwner().getAvatar())
                .placeholder(ResourcesCompat.getDrawable(
                        context.getResources(),
                        R.drawable.ic_person_black_24dp, null))
                .into(avatarImage);
    }
}
