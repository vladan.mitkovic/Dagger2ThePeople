package me.mitkovic.android.dagger2thepeople.examples.rxjava2.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import me.mitkovic.android.dagger2thepeople.R;
import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;

public class RxJava2Activity extends AppCompatActivity implements RxJava2View {

    @Inject
    RxJava2Presenter presenter;

    @BindView(R.id.repo_list)
    RecyclerView recyclerView;

    @Inject
    ReposAdapter reposAdapter;

    @BindView(R.id.loading)
    View loading;

    @BindView(R.id.layout_holder)
    RelativeLayout layoutHolder;

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        setContentView(R.layout.activity_rxjava2);
        ButterKnife.bind(this);

        setTitle(getString(R.string.activity_rxjava2));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(reposAdapter);

        if (savedInstanceState != null) {
            presenter.onRestore(savedInstanceState);
        }
        presenter.onBind(this);
    }

    @Override
    public void setAdapterData(List<GithubRepo> githubRepos) {
        reposAdapter.swapData(githubRepos);
    }

    @Override
    public void setSuccessSnackBar() {
        String successText = getString(R.string.success_message);
        Snackbar snackbar = Snackbar.make(layoutHolder, successText, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void setErrorMessage(String errorMessage) {
        Toast.makeText(RxJava2Activity.this, getString(R.string.error_repos_message) + errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoading(boolean isLoading) {
        loading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            super.onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.refresh) {
            reposAdapter.clearData();
            presenter.onClickRefresh();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null)
            presenter.onSave(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null)
            presenter.onUnbind();
    }

}
