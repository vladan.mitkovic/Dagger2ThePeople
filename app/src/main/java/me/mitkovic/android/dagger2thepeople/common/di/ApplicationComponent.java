package me.mitkovic.android.dagger2thepeople.common.di;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import me.mitkovic.android.dagger2thepeople.common.PeopleApplication;
import me.mitkovic.android.dagger2thepeople.common.di.module.APIModule;
import me.mitkovic.android.dagger2thepeople.common.di.module.AppModule;
import me.mitkovic.android.dagger2thepeople.common.di.module.ApplicationModule;
import me.mitkovic.android.dagger2thepeople.common.di.scope.PeopleApplicatonScope;

@PeopleApplicatonScope
@Component(modules = {
        AndroidInjectionModule.class,
        ApplicationModule.class,
        APIModule.class,
        AppModule.class
})
public interface ApplicationComponent {

    void inject(PeopleApplication peopleApplication);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(PeopleApplication application);
        ApplicationComponent build();
    }

}
