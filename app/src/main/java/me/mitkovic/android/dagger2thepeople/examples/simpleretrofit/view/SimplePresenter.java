package me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view;

import me.mitkovic.android.dagger2thepeople.common.view.Presenter;

public interface SimplePresenter extends Presenter<SimpleView> {

    void onClickRefresh();

}
