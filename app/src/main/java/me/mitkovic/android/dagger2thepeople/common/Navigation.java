package me.mitkovic.android.dagger2thepeople.common;

public interface Navigation {

    void viewSimpleExample();

    void viewRxJava2Example();

}
