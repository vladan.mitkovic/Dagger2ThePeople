package me.mitkovic.android.dagger2thepeople.examples.rxjava2.view;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.mitkovic.android.dagger2thepeople.R;
import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;
import me.mitkovic.android.dagger2thepeople.examples.rxjava2.view.RxJava2Activity.ItemClickListener;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ReposViewHolder> implements ItemClickListener {

    private List<GithubRepo> repoList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private RxJava2Activity context;

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.fullDate();

    @Inject
    public ReposAdapter(RxJava2Activity context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ReposAdapter.ReposViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ReposViewHolder(layoutInflater.inflate(R.layout.list_item_repo_fresco, viewGroup, false), this);
    }

    @Override
    public void onBindViewHolder(ReposViewHolder viewHolder, int i) {
        viewHolder.name.setText(repoList.get(i).getName());
        viewHolder.description.setText(repoList.get(i).getDescription());
        viewHolder.updatedAt.setText(DATE_TIME_FORMATTER.print(repoList.get(i).getUpdatedAt()));
        viewHolder.useAvatar.setImageURI(Uri.parse(repoList.get(i).getOwner().getAvatar()));
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(context, context.getString(R.string.repository_name_message) + repoList.get(position).getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return repoList.size();
    }

    public void swapData(List<GithubRepo> githubRepos) {
        repoList.clear();
        if(githubRepos != null) {
            repoList.addAll(githubRepos);
        }
        notifyDataSetChanged();
    }

    public void clearData() {
        repoList.clear();
        notifyDataSetChanged();
    }

    public class ReposViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.repo_name)
        TextView name;

        @BindView(R.id.user_avatar)
        SimpleDraweeView useAvatar;

        @BindView(R.id.repo_description)
        TextView description;

        @BindView(R.id.repo_updated_at)
        TextView updatedAt;

        ItemClickListener itemClickListener;

        public ReposViewHolder(View view, ItemClickListener itemClickListener) {
            super(view);
            ButterKnife.bind(this, view);

            this.itemClickListener = itemClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(view, getAdapterPosition());
        }

    }

}
