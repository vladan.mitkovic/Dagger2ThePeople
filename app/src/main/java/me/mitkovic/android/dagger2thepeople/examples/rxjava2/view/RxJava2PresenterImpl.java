package me.mitkovic.android.dagger2thepeople.examples.rxjava2.view;

import android.os.Bundle;

import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import icepick.Icepick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import me.mitkovic.android.dagger2thepeople.common.api.GitHubAPI;
import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;

public class RxJava2PresenterImpl implements RxJava2Presenter {

    private RxJava2View view;

    private GitHubAPI githubService;

    private CompositeDisposable subscription;

    @Inject
    public RxJava2PresenterImpl(GitHubAPI githubService) {
        this.githubService = githubService;
    }

    @Override
    @DebugLog
    public void onBind(final RxJava2View view) {
        this.view = view;

        setLoading(true);

        subscription = new CompositeDisposable();

        getData();
    }

    private void getData() {
        Disposable serviceSubscription = githubService.getObservableRepos()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(successConsumer(), errorConsumer());

        subscription.add(serviceSubscription);
    }

    private Consumer<List<GithubRepo>> successConsumer() {
        return new Consumer<List<GithubRepo>>() {

            @Override
            @DebugLog
            public void accept(final List<GithubRepo> githubRepos) throws Exception {
                setLoading(false);
                view.setSuccessSnackBar();
                view.setAdapterData(githubRepos);
            }
        };
    }

    private Consumer<Throwable> errorConsumer() {
        return new Consumer<Throwable>() {

            @Override
            @DebugLog
            public void accept(Throwable throwable) throws Exception {
                setLoading(false);
                view.setErrorMessage(throwable.getMessage());
            }
        };
    }

    @Override
    public void onClickRefresh() {
        setLoading(true);
        getData();
    }

    private void setLoading(boolean loading) {
        view.setLoading(loading);
    }

    @Override
    public void onUnbind() {
        this.view = null;

        if (subscription != null) {
            subscription.dispose();
            subscription = null;
        }
    }

    @Override
    public void onRestore(Bundle bundle) {
        Icepick.restoreInstanceState(this, bundle);
    }

    @Override
    public void onSave(Bundle bundle) {
        Icepick.saveInstanceState(this, bundle);
    }

}
