package me.mitkovic.android.dagger2thepeople.common.di.module;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.dagger2thepeople.BuildConfig;
import me.mitkovic.android.dagger2thepeople.common.api.GitHubAPI;
import me.mitkovic.android.dagger2thepeople.common.di.scope.ApplicationContext;
import me.mitkovic.android.dagger2thepeople.common.di.scope.PeopleApplicatonScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class APIModule {

    @Provides
    @PeopleApplicatonScope
    public OkHttpClient providesOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        HttpLoggingInterceptor headerLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        HttpLoggingInterceptor bodyLogging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(headerLogging)
                .addInterceptor(bodyLogging)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @PeopleApplicatonScope
    public Gson providesGson(GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @Provides
    @PeopleApplicatonScope
    public GsonBuilder providesGsonBuilder() {
        return new GsonBuilder();
    }

    @Provides
    @PeopleApplicatonScope
    public Retrofit retrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .baseUrl(BuildConfig.GITHUB_API_ENDPOINT)
                .build();
    }

    @Provides
    @PeopleApplicatonScope
    public GitHubAPI githubService(Retrofit githubRetrofit) {
        return githubRetrofit.create(GitHubAPI.class);
    }

    @Provides
    @PeopleApplicatonScope
    public Picasso picasso(@ApplicationContext Context context, OkHttp3Downloader okHttp3Downloader) {
        return new Picasso.Builder(context)
                .downloader(okHttp3Downloader)
                .build();
    }

    @Provides
    @PeopleApplicatonScope
    public OkHttp3Downloader okHttp3Downloader(OkHttpClient okHttpClient) {
        return new OkHttp3Downloader(okHttpClient);
    }

}
