package me.mitkovic.android.dagger2thepeople.common.di.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import me.mitkovic.android.dagger2thepeople.common.di.scope.PerActivity;
import me.mitkovic.android.dagger2thepeople.examples.rxjava2.di.RxJava2Module;
import me.mitkovic.android.dagger2thepeople.examples.rxjava2.view.RxJava2Activity;
import me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.di.SimpleModule;
import me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view.SimpleActivity;
import me.mitkovic.android.dagger2thepeople.home.di.MainModule;
import me.mitkovic.android.dagger2thepeople.home.view.MainActivity;

@Module
public abstract class AppModule {

    @PerActivity
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = SimpleModule.class)
    abstract SimpleActivity simpleActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = RxJava2Module.class)
    abstract RxJava2Activity rxJava2ActivityInjector();
}
