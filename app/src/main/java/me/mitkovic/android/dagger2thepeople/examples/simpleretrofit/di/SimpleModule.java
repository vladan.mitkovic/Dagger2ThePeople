package me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.di;


import dagger.Binds;
import dagger.Module;
import me.mitkovic.android.dagger2thepeople.common.di.scope.PerActivity;
import me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view.SimplePresenter;
import me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view.SimplePresenterImpl;

@PerActivity
@Module
public abstract class SimpleModule {

    @Binds
    abstract SimplePresenter bindsSimplePresenter(SimplePresenterImpl presenter);

}
