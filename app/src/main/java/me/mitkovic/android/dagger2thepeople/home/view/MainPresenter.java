package me.mitkovic.android.dagger2thepeople.home.view;

import me.mitkovic.android.dagger2thepeople.common.view.Presenter;

public interface MainPresenter extends Presenter<MainView> {

    void onClickSimpleExample();

    void onClickRxJava2Example();

}
