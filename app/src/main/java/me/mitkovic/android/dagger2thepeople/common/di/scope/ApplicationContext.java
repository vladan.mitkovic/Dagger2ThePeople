package me.mitkovic.android.dagger2thepeople.common.di.scope;

import javax.inject.Qualifier;

@Qualifier
public @interface ApplicationContext {
}
