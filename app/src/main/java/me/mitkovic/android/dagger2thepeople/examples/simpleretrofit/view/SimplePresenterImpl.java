package me.mitkovic.android.dagger2thepeople.examples.simpleretrofit.view;

import android.os.Bundle;

import java.util.List;

import javax.inject.Inject;

import hugo.weaving.DebugLog;
import icepick.Icepick;
import me.mitkovic.android.dagger2thepeople.common.api.GitHubAPI;
import me.mitkovic.android.dagger2thepeople.common.api.model.GithubRepo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SimplePresenterImpl implements SimplePresenter {

    private SimpleView view;
    private GitHubAPI githubService;

    @Inject
    public SimplePresenterImpl(GitHubAPI githubService) {
        this.githubService = githubService;
    }

    @Override
    @DebugLog
    public void onBind(final SimpleView view) {
        this.view = view;

        setLoading(true);

        getData();
    }

    private void getData() {
        Call<List<GithubRepo>> reposCall = githubService.getAllRepos();
        reposCall.enqueue(new Callback<List<GithubRepo>>() {

            @Override
            @DebugLog
            public void onResponse(Call<List<GithubRepo>> call, Response<List<GithubRepo>> response) {
                setLoading(false);
                view.setSuccessSnackBar();
                view.setAdapterData(response.body());
            }

            @Override
            @DebugLog
            public void onFailure(Call<List<GithubRepo>> call, Throwable throwable) {
                setLoading(false);
                view.setErrorMessage(throwable.getMessage());
            }
        });
    }

    @Override
    public void onClickRefresh() {
        setLoading(true);
        getData();
    }

    private void setLoading(boolean loading) {
        view.setLoading(loading);
    }

    @Override
    public void onUnbind() {
        this.view = null;
    }

    @Override
    public void onRestore(Bundle bundle) {
        Icepick.restoreInstanceState(this, bundle);
    }

    @Override
    public void onSave(Bundle bundle) {
        Icepick.saveInstanceState(this, bundle);
    }
}
