package me.mitkovic.android.dagger2thepeople.home.di;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import me.mitkovic.android.dagger2thepeople.common.Navigation;
import me.mitkovic.android.dagger2thepeople.common.NavigationImpl;
import me.mitkovic.android.dagger2thepeople.common.di.scope.PerActivity;
import me.mitkovic.android.dagger2thepeople.home.view.MainActivity;
import me.mitkovic.android.dagger2thepeople.home.view.MainPresenter;
import me.mitkovic.android.dagger2thepeople.home.view.MainPresenterImpl;

@PerActivity
@Module
public abstract class MainModule {

    @Binds
    abstract MainPresenter bindsMainPresenter(MainPresenterImpl presenter);

    @PerActivity
    @Provides
    public static Navigation providesNavigation(MainActivity mainActivity) {
        return new NavigationImpl(mainActivity);
    }

}
